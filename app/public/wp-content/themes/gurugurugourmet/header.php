<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns#">

<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">

	<title>gurugurugourmet</title>
	<meta name="description" content="">

	<meta property="og:title" content="gurugurugourmet">
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://example.com/">
	<meta property="og:image" content="https://example.com/img/ogp.png">
	<meta property="og:site_name" content="gurugurugourmet">
	<meta property="og:description" content="">
	<meta name="twitter:card" content="summary_large_image">

  <?php wp_head(); ?>
	<link rel="icon" href="./img/icon-home.png">

</head>

<body>

	<!-- header -->
	<header id="header">
		<div class="inner">
			<?php if (is_home() || is_front_page() ): //トップページではh1でそれ以外ではdiv?>
			<h1 class="header-logo">
				<a href="<?php echo esc_url(home_url('/'))?>">
					<?php bloginfo('name'); ?>
				</a>
			</h1>
			<?php else : ?>
			<div class="header-logo">
				<a href="<?php echo esc_url(home_url('/')) ?>">
					<?php bloginfo('name'); ?>
				</a>
			</div>
			<?php endif; ?>
			<div class="header-sub">
				<?php bloginfo('description'); //プログのdescriptionを表示 ?>
			</div>


			<!-- drawer -->
			<div class="drawer">
				<div class="drawer-icon">
					<span class="drawer-open"><i class="fas fa-bars"></i></span><!-- /drawer-open -->
					<span class="drawer-close"><i class="fas fa-times"></i></span><!-- drawer-close -->
				</div><!-- /drawer-icon -->

				<!-- drawer-content -->
				<div class="drawer-content">
					<div class="drawer-nav">
						<div class="drawer-title">
							<p>シーン</p>
							<span class="nav-open"><i class="fas fa-sort-up"></i></span>
							<span class="nav-close"><i class="fas fa-sort-down"></i></span>
						</div>
						<ul class="drawer-list">
						<?php 
							$categories = get_categories();
							if (!empty($categories)) {
								foreach ($categories as $key => $term) {
									echo
									'<li><a href="' . get_category_link($term->term_id) . '" class="widget-item">' . $term->name . '(' . $term->count . ')' . '</a></li>';
								}
							}
						?>
						</ul>
					</div>
					<!-- CPTで作成したタクソノミー一覧を表示 -->
					<?php get_hamburger_terms('genres'); ?>
					<?php get_hamburger_terms('station'); ?>
					<!-- end CPTで作成したタクソノミー一覧を表示 -->
				</div>
			</div><!-- /drawer -->

		</div><!-- /inner -->
	</header><!-- /header -->

	<!-- header-nav -->
	<nav class="header-nav">
		<div class="inner">
		<?php
			wp_nav_menu(
				array(
					'depth' => 1,
					'theme_location' => 'header',
					'container' => 'false',
					'menu_class' => 'header-list',
				)
			);
		?>
		</div><!-- /inner -->
	</nav><!-- header-nav -->
