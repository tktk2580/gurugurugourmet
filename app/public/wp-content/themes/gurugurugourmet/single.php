<?php get_header(); ?>

	<!-- content -->
	<div id="content" class="content-single-pc">
		<div class="inner">

			<!-- primary -->
			<main id="primary">
        <?php if ( function_exists('bcn_display')):?>
        <!-- breadcrumb -->
				<div class="breadcrumb">
          <?php bcn_display();?>
        </div><!-- /breadcrumb -->
        <?php endif; ?>

        <?php
        if (have_posts()):
        while(have_posts()):
          the_post();
        ?>

				<!-- entry single-pc -->
				<article <?php post_class(array('entry'));?>>

					<!-- entry-header -->
					<div class="entry-header">
            <?php
            $category = get_the_category();

            if ($category[0]): ?>
            <div class="entry-label"><a href="<?php echo esc_url(get_category_link($category[0]->cat_ID)); ?>"><?php echo $category[0]->name ?></a></div><!-- /entry-item-tag -->
            <?php endif; ?>
						<h1 class="entry-title"><?php the_title(); ?></h1><!-- /entry-title -->

						<!-- entry-meta -->
						<div class="entry-item-meta">
							<div class="entry-item-centerBox">
								<?php
									// 最寄り駅
									$stations = my_the_post_taxonomy($post->ID, 'station');
									if (!empty($stations)) {
										foreach ($stations as $term) {
											echo '<p class="single-item-station-title">最寄り駅 :</p>';
											echo '<a href="' . get_term_link($term->slug, 'station') . '" class="single-item-centerBox-factor single-item-station">' . $term->name . '駅</a>';
										}
									}
								?>
								<?php
									// 都道府県
									$prefectures = my_the_post_taxonomy($post->ID, 'prefectures');
									if (!empty($prefectures)) {
										foreach ($prefectures as $term) {
												echo '<p class="entry-item-prefecture">[' . $term->name . ']</p>';
										}
									}
								?>
								<?php
									// ジャンル
									$genres = my_the_post_taxonomy($post->ID, 'genres');
									if (!empty($genres)) {
										// max5件表示
										array_splice($genres, 5);
										echo '<p class="single-item-genre-title">ジャンル :</p>';
										foreach ($genres as $key => $term) {
											if ($key === array_key_last($genres)) {
												echo '<a href="' . get_term_link($term->slug, 'genres') . '" class="single-item-centerBox-factor">' . $term->name . '</a>';
												break;
											}
											echo '<a href="' . get_term_link($term->slug, 'genres') . '" class="single-item-centerBox-factor single-item-genre">' . $term->name . '</a>';
											echo '、';
										}
									}
								?>
							</div>
							<div class="entry-item-priceBox single-item-priceBox">
								<p class="single-item-price-title">予算 :</p>
								<!-- 価格(夜) -->
								<div class="entry-item-dinner entry-item-price">
									<i class="fas fa-moon"></i>
									<?php
										$dinnerPrices = my_the_post_taxonomy($post->ID, 'dinner_prices');
										if (!empty($dinnerPrices)) {
											echo '<p> ¥' . number_format($dinnerPrices[0]->name) . ' ~ ¥' . number_format($dinnerPrices[1]->name) . '</p>';
										} else {
											echo '-';
										}
									?>
								</div>
								<!-- 価格(昼) -->
								<div class="entry-item-lunch entry-item-price">
									<i class="fas fa-sun"></i>
									<?php
										$lunchPrices = my_the_post_taxonomy($post->ID, 'lunch_prices');
										if (!empty($lunchPrices)) {
											echo '<p> ¥' . number_format($lunchPrices[0]->name) . ' ~ ¥' . number_format($lunchPrices[1]->name) . '</p>';
										} else {
											echo '-';
										}
									?>
								</div>
							</div>
							<time class="entry-published" datetime="<?php the_time('c') ?>">公開日 <?php the_time('Y-m-d'); ?></time>
							<time class="entry-updated" datetime="<?php the_modified_time('c'); ?>">最終更新日 <?php the_modified_time('Y-m-d'); ?></time>
						</div><!-- /entry-meta -->

						<!-- entry-img -->
						<div class="entry-img">
              <?php
              if ( has_post_thumbnail() ) {
                the_post_thumbnail('large');
              }
              ?>
						</div><!-- /entry-img -->
					</div><!-- /entry-header -->

					<!-- entry-body -->
					<div class="entry-body">
            <?php
            the_content();
            ?>
            <?php
            wp_link_pages(
            array(
              'before' => '<nav class="entry-links">',
              'after' => '</nav>',
              'link_before' => '',
              'link_after' => '',
              'next_or_number' => 'number',
              'separator' => '',
            )
            );
            ?>
					</div><!-- /entry-body -->
					
					<!-- カテゴリー検索など -->
					<?php get_template_part('template-parts/related_search');?>

					<!-- 関連記事(カテゴリーで関連づけている) -->
					<?php get_template_part('template-parts/related');?>
        </article> <!-- /entry single-pc -->

        <?php
        endwhile;
        endif;
        ?>
			</main><!-- /primary -->

      <?php get_sidebar();?>

		</div><!-- /inner -->
	</div><!-- /content -->

	<div id="content-single-sp" class="content-single-sp">
		<div class="inner-single">

			<!-- primary -->
			<main id="primary">
        <?php if ( function_exists('bcn_display')):?>
        <!-- breadcrumb -->
				<div class="breadcrumb">
          <?php bcn_display();?>
        </div><!-- /breadcrumb -->
        <?php endif; ?>

        <?php
        if (have_posts()):
        while(have_posts()):
          the_post();
        ?>

				<!-- single-sp -->
				<article <?php post_class(array('entry', 'single-sp'));?>>
					<!-- entry-header -->
					<div class="entry-header">
						<!-- entry-img -->
						<div class="entry-img">
              <?php
              if ( has_post_thumbnail() ) {
                the_post_thumbnail('large');
              }
              ?>
						</div><!-- /entry-img -->
						<!-- entry-meta -->
						<div class="entry-item-meta">
							<?php
							$category = get_the_category();
							if ($category[0]): ?>
							<div class="entry-label"><a href="<?php echo esc_url(get_category_link($category[0]->cat_ID)); ?>"><?php echo $category[0]->name ?></a></div><!-- /entry-item-tag -->
							<?php endif; ?>
							<h1 class="entry-title"><?php the_title(); ?></h1><!-- /entry-title -->
							<div class="entry-item-centerBox">
								<?php
									// 最寄り駅
									$stations = my_the_post_taxonomy($post->ID, 'station');
									if (!empty($stations)) {
										foreach ($stations as $term) {
											echo '<p class="single-item-station-title">最寄り駅 :</p>';
											echo '<a href="' . get_term_link($term->slug, 'station') . '" class="single-item-centerBox-factor single-item-station">' . $term->name . '駅</a>';
										}
									}
								?>
								<?php
									// 都道府県
									$prefectures = my_the_post_taxonomy($post->ID, 'prefectures');
									if (!empty($prefectures)) {
										foreach ($prefectures as $term) {
												echo '<p class="entry-item-prefecture">[' . $term->name . ']</p>';
										}
									}
								?>
								<?php
									// ジャンル
									$genres = my_the_post_taxonomy($post->ID, 'genres');
									if (!empty($genres)) {
										// max5件表示
										array_splice($genres, 5);
										echo '<p class="single-item-genre-title">ジャンル :</p>';
										foreach ($genres as $key => $term) {
											if ($key === array_key_last($genres)) {
												echo '<a href="' . get_term_link($term->slug, 'genres') . '" class="single-item-centerBox-factor">' . $term->name . '</a>';
												break;
											}
											echo '<a href="' . get_term_link($term->slug, 'genres') . '" class="single-item-centerBox-factor">' . $term->name . '</a>';
											echo '、';
										}
									}
								?>
							</div>
							<div class="entry-item-priceBox single-item-priceBox">
								<p class="single-item-price-title">予算 :</p>
								<!-- 価格(夜) -->
								<div class="entry-item-dinner entry-item-price">
									<i class="fas fa-moon"></i>
									<?php
										$dinnerPrices = my_the_post_taxonomy($post->ID, 'dinner_prices');
										if (!empty($dinnerPrices)) {
											echo '<p> ¥' . number_format($dinnerPrices[0]->name) . ' ~ ¥' . number_format($dinnerPrices[1]->name) . '</p>';
										} else {
											echo '-';
										}
									?>
								</div>
								<!-- 価格(昼) -->
								<div class="entry-item-lunch entry-item-price">
									<i class="fas fa-sun"></i>
									<?php
										$lunchPrices = my_the_post_taxonomy($post->ID, 'lunch_prices');
										if (!empty($lunchPrices)) {
											echo '<p> ¥' . number_format($lunchPrices[0]->name) . ' ~ ¥' . number_format($lunchPrices[1]->name) . '</p>';
										} else {
											echo '-';
										}
									?>
								</div>
							</div>
							<time class="entry-published" datetime="<?php the_time('c') ?>">公開日 <?php the_time('Y-m-d'); ?></time>
							<time class="entry-updated" datetime="<?php the_modified_time('c'); ?>">最終更新日 <?php the_modified_time('Y-m-d'); ?></time>
						</div><!-- /entry-meta -->
					</div><!-- /entry-header -->
					<hr class="hr-single-sp">
					<!-- entry-body -->
					<div class="entry-body">
            <?php
            the_content();
            ?>
            <?php
            wp_link_pages(
            array(
              'before' => '<nav class="entry-links">',
              'after' => '</nav>',
              'link_before' => '',
              'link_after' => '',
              'next_or_number' => 'number',
              'separator' => '',
            )
            );
            ?>
					</div><!-- /entry-body -->
					<div class="entry-footer">

						<!-- カテゴリー検索など -->
						<?php get_template_part('template-parts/related_search');?>

						<!-- 関連記事(カテゴリーで関連づけている) -->
						<?php get_template_part('template-parts/related');?>
					</div>
        </article> <!-- /entry -->

        <?php
        endwhile;
        endif;
        ?>
			</main><!-- /primary -->

			<!-- なぜか表示されないsidebar -->
			<?php get_sidebar(); ?>

		</div><!-- /inner -->
	</div><!-- /content -->
  <?php get_footer(); ?>
