<!-- pickup -->
<div id="pickup">
  <h1>PickUp</h1>
  <div class="inner">
    <div class="pickup-items">
    <?php
    $pickup_posts = get_posts( array(
      'post_type' => 'post',
      'posts_per_page' => '3',
      'tag' => 'pickup',
      'orderby' => 'DESC',
    ));
    ?>
    <?php foreach($pickup_posts as $post):setup_postdata($post); ?>
      <a href="<?php echo esc_url(get_permalink());?>" class="pickup-item">
        <div class="pickup-item-img">
        <?php
        if (has_post_thumbnail()) {
          the_post_thumbnail('large');
        } else {
          echo '<img src="'.esc_url(get_template_directory_uri()).'img/noimg.png alt="">';
        }
        ?>
          <div class="pickup-item-tag"><?php my_the_post_category(false);?></div><!-- /pickup-item-tag -->
        </div><!-- /pickup-item-img -->
        <div class="pickup-item-body">
          <h2 class="pickup-item-title"><?php echo esc_html(get_the_title());?></h2><!-- /pickup-item-title -->
          <?php
            // サブタイトル
            $subtitle = my_the_post_taxonomy($post->ID, 'subtitle');
            if (!empty($subtitle)) {
              foreach ($subtitle as $term) {
                  echo '<h3>' . $term->name . '</h3>';
              }
            }
          ?>
          <?php
            // 都道府県
            $prefectures = my_the_post_taxonomy($post->ID, 'prefectures');
            if (!empty($prefectures)) {
              foreach ($prefectures as $term) {
                  echo '<p>' . $term->name . '</p>';
              }
            }
          ?>
          <?php
            // 最寄り駅
            $stations = my_the_post_taxonomy($post->ID, 'station');
            if (!empty($stations)) {
                foreach ($stations as $term) {
                    echo '<p>' . $term->name . '</p>';
                }
            }
          ?>
        </div><!-- /pickup-item-body -->
      </a><!-- /pickup-item -->
      <?php endforeach;wp_reset_postdata(); ?>

    </div><!-- /pickup-items -->
  </div><!-- /inner -->
</div><!-- /pickup -->
