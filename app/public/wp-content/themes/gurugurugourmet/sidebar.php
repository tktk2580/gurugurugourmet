<aside id="secondary">
  <div class="sidebar-search">
    <div class="sidebar-widget">
      <div class="widget-title">シーン</div>
      <?php 
        $categories = get_categories();
        if (!empty($categories)) {
          foreach ($categories as $key => $term) {
            echo '<a href="' . get_category_link($term->term_id) . '" class="widget-item">' . $term->name . '(' . $term->count . ')' . '</a>';
          }
        }
      ?>
    </div>
    <!-- CPTで作成したタクソノミー一覧を表示 -->
    <?php get_sidebar_terms('genres'); ?>
    <?php get_sidebar_terms('station'); ?>
    <!-- end CPTで作成したタクソノミー一覧を表示 -->
  </div>
  <?php if(is_active_sidebar('sidebar')):?>
    <?php dynamic_sidebar('sidebar');?>
  <?php endif;?>
</aside>
