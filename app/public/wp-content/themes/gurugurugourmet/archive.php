<?php get_header(); ?>


	<!-- content -->
	<div id="content">
		<div class="inner">

			<!-- primary -->
			<main id="primary">

				<div class="archive-head m_description">
					<?php 
					$query_object = get_queried_object();
					$taxonomy_slug = $query_object->taxonomy;
					echo '<div class="archive-lead">' . strtoupper($taxonomy_slug) . '</div>';
					?>
					<h1 class="archive-title m_category">
            <?php the_archive_title(); ?>
          </h1><!-- /archive-title -->
					<div class="archive-description">
						<p>
							<?php the_archive_description(); ?>
						</p>
					</div><!-- /archive-description -->
				</div><!-- /archive-head -->

        <?php
        if (have_posts()):
        ?>
				<!-- entries -->
				<div class="entries m_horizontal">
        <?php
        while(have_posts()):
          the_post();
        ?>
          <!-- entry-item -->
					<a href="<?php the_permalink();?>" class="entry-item">
						<!-- entry-item-img -->
						<div class="entry-item-img entry-item-category-img">
              <?php
              if(has_post_thumbnail()){
                the_post_thumbnail('large');
              } else {
                echo '<img src="' .esc_url(get_template_directory_uri() . 'img/noimg.png alt="">');
              }
              ?>
						</div><!-- /entry-item-img -->

						<!-- entry-item-body -->
						<div class="entry-item-body">
							<div class="entry-item-meta">
              	<!-- trueを引数として渡すとリンク付き、falseを渡すとリンクなし -->
								<!-- <div class="entry-item-tag">
									<?php my_the_post_category( false );?>
								</div> -->
								<div class="entry-item-topBox">
              		<h2 class="entry-item-title"><?php echo esc_html(get_the_title());?></h2>
								</div>
								<div class="entry-item-centerBox">
									<?php
										// 都道府県
										$prefectures = my_the_post_taxonomy($post->ID, 'prefectures');
										if (!empty($prefectures)) {
											foreach ($prefectures as $term) {
													echo '<p class="entry-item-prefecture">[' . $term->name . ']</p>';
											}
										}
									?>
									<?php
										// 最寄り駅
										$stations = my_the_post_taxonomy($post->ID, 'station');
										if (!empty($stations)) {
											foreach ($stations as $term) {
												echo '<p class="entry-item-station">' . $term->name . '駅</p>';
											}
										}
									?>
									<p class="entry-item-span">/</p>
									<?php
										// ジャンル
										$genres = my_the_post_taxonomy($post->ID, 'genres');
										if (!empty($genres)) {
											// max5件表示
											array_splice($genres, 5);
											foreach ($genres as $key => $term) {
												if ($key === array_key_last($genres)) {
													echo '<p class="entry-item-station">' . $term->name . '</p>';
													break;
												}
												echo '<p class="entry-item-station">' . $term->name . '、 </p>';
											}
										}
									?>
								</div>
								<div class="entry-item-priceBox category-item-priceBox">
									<!-- 価格(夜) -->
									<div class="archive-item-dinner entry-item-price">
										<i class="fas fa-moon"></i>
										<?php
											$dinnerPrices = my_the_post_taxonomy($post->ID, 'dinner_prices');
											if (!empty($dinnerPrices)) {
												echo '<p class="category-price-pc"> ¥' . number_format($dinnerPrices[0]->name) . ' ~ ¥' . number_format($dinnerPrices[1]->name) . '</p>';
												echo '<p class="category-price-sp"> ¥' . number_format($dinnerPrices[0]->name) . ' ~' . '</p>';
											} else {
												echo '-';
											}
										?>
									</div>
									<!-- 価格(昼) -->
									<div class="entry-item-lunch entry-item-price">
										<i class="fas fa-sun"></i>
										<?php
											$lunchPrices = my_the_post_taxonomy($post->ID, 'lunch_prices');
											if (!empty($lunchPrices)) {
												echo '<p class="category-price-pc"> ¥' . number_format($lunchPrices[0]->name) . ' ~ ¥' . number_format($lunchPrices[1]->name) . '</p>';
												echo '<p class="category-price-sp"> ¥' . number_format($lunchPrices[0]->name) . ' ~' . '</p>';
											} else {
												echo '-';
											}
										?>
									</div>
								</div>
							</div><!-- /entry-item-meta -->
							<div class="entry-item-excerpt">
								<?php the_excerpt();?>
							</div>
						</div><!-- /entry-item-body -->
						<!-- <div class="entry-item-excerpt"> -->
						<div class="category-item-excerpt-sp">
							<?php the_excerpt();?>
						</div>
					</a><!-- /entry-item -->

        <?php
        endwhile;
        ?>
        </div>
				<?php endif; ?>
				<!-- pagenation -->
				<?php get_template_part('template-parts/pagenation');?>
			</main><!-- /primary -->

			<?php get_sidebar(); ?>


		</div><!-- /inner -->
	</div><!-- /content -->

	<?php get_footer(); ?>
