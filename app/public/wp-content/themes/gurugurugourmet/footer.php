
	<!-- footer-menu -->
	<div id="footer-menu">
		<div class="inner">

      <?php if (is_home() || is_front_page() ) : //トップページではロゴをh1に、それ以外のページではdivに。 ?>
        <div class="footer-logo"><a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></div><!-- /header-logo -->
      <?php else : ?>
        <div class="footer-logo"><a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></div><!-- /header-logo -->
      <?php endif; ?>
      <div class="footer-sub"><?php bloginfo('description'); //ブログのdescriptionを表示 ?></div><!-- /header-sub -->
      <!-- footer-nav -->
      <nav class="footer-nav">
        <div class="inner">
          <?php
          wp_nav_menu(
          //.footer-listを置き換えて、PC用メニューを動的に表示する
          array(
          'depth' => 1,
          'theme_location' => 'footer', //グローバルメニューをここに表示すると指定
          'container' => 'false',
          'menu_class' => 'footer-list',
          )
          );
          ?>
        </div><!-- /inner -->
      </nav><!-- header-nav -->

		</div><!-- /inner -->
	</div><!-- /footer-menu -->



	<!-- footer -->
	<footer id="footer">
		<div class="inner">
			<div class="copy">&copy; T.K of world All rights reserved.</div><!-- /copy -->
			<div class="by">Presented by GURUGURUGOURMET
			</div><!-- /by -->

		</div><!-- /inner -->
	</footer><!-- /footer -->

  <?php get_template_part('template-parts/share-btn'); ?>

	<div class="floating">
		<a href="#"><i class="fas fa-chevron-up"></i></a>
	</div>

  <?php if(is_single()): ?>
  <div class="footer-sns">
    <div class="inner">
      <div class="footer-sns-head">この記事をシェアする</div>
      <nav class="footer-sns-buttons">
        <ul>
          <li><a class="sns__twitter" href="https://twitter.com/share?url=<? echo get_the_permalink(); ?>&text=<? echo get_the_title(); ?>" target="_blank" rel="nofollow noopener"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-twitter.png" alt=""></a>
          </li>
          <li><a class="sns__facebook" href="http://www.facebook.com/share.php?u=<? echo get_the_permalink(); ?>" target="_blank" rel="nofollow noopener"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt=""></a>
          </li>
          <li><a class="sns__hatena" href="http://b.hatena.ne.jp/entry/<?php the_permalink(); ?>" target="_blank" rel="nofollow noopener"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-hatena.png" alt=""></a>
          </li>
          <li><a class="sns__line" href="https://social-plugins.line.me/lineit/share?url=<? echo get_the_permalink(); ?>"target="_blank" rel="nofollow noopener"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-line.png" alt=""></a>
          </li>
          <li><a class="sns__pocket" href="http://getpocket.com/edit?url=<? echo get_the_permalink();?>&title=<? echo get_the_title(); ?>" target="_blank" rel="nofollow noopener"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-pocket.png" alt=""></a>
          </li>
        </ul>
      </nav>

    </div>
  </div>
  <?php endif; ?>
</div>
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="js/script.js"></script> -->

<?php wp_footer(); ?>
</body>

</html>
