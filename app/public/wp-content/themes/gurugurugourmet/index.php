<?php get_header(); ?>
	<!-- pickup -->
	<?php get_template_part('template-parts/pickup_by_tag');?>

	<!-- content -->
	<div id="content">
		<div class="inner">

			<!-- primary -->
			<main id="primary">

			<?php
			if (have_posts()):
			?>

				<!-- entries -->
				<div class="entries">
				<?php
				while(have_posts()):
					the_post();
				?>

					<!-- entry-item -->
					<a href="<?php the_permalink();?>" class="entry-item">
						<!-- entry-item-img -->
						<div class="entry-item-img">
							<?php
							if(has_post_thumbnail()){
								the_post_thumbnail(array(320,270));
							} else {
								echo '<img src="' . esc_url(get_template_directory_uri()) . 'img/noimg.png" alt="">';
							}
							?>
							<!-- <div class="entry-item-tag">
								<?php my_the_post_category( false );?>
							</div> -->
						</div><!-- /entry-item-img -->

						<!-- entry-item-body -->
						<div class="entry-item-body">
							<div class="entry-item-meta">
								<div class="entry-item-topBox">
									<h2 class="entry-item-title"><?php echo esc_html(get_the_title());?></h2>
									<time class="entry-item-published" datetime="<?php the_time('c'); ?>"><?php the_time('Y/n/j'); ?></time><!-- /entry-item-published -->
								</div>
								<div class="entry-item-centerBox">
									<?php
										// 都道府県
										$prefectures = my_the_post_taxonomy($post->ID, 'prefectures');
										if (!empty($prefectures)) {
											foreach ($prefectures as $term) {
													echo '<p class="entry-item-prefecture">[' . $term->name . ']</p>';
											}
										}
									?>
									<?php
										// 最寄り駅
										$stations = my_the_post_taxonomy($post->ID, 'station');
										if (!empty($stations)) {
											foreach ($stations as $term) {
												echo '<p class="entry-item-station">' . $term->name . '駅</p>';
											}
										}
									?>
									<p class="entry-item-span">/</p>
									<?php
										// ジャンル
										$genres = my_the_post_taxonomy($post->ID, 'genres');
										if (!empty($genres)) {
											// max5件表示
											array_splice($genres, 5);
											foreach ($genres as $key => $term) {
												if ($key === array_key_last($genres)) {
													echo '<p class="entry-item-station">' . $term->name . '</p>';
													break;
												}
												echo '<p class="entry-item-station">' . $term->name . '、 </p>';
											}
										}
									?>
								</div>
								<div class="entry-item-priceBox">
									<!-- 価格(夜) -->
									<div class="entry-item-dinner entry-item-price">
										<i class="fas fa-moon"></i>
										<?php
											$dinnerPrices = my_the_post_taxonomy($post->ID, 'dinner_prices');
											if (!empty($dinnerPrices)) {
												echo '<p> ¥' . number_format($dinnerPrices[0]->name) . ' ~ ¥' . number_format($dinnerPrices[1]->name) . '</p>';
											} else {
												echo '-';
											}
										?>
									</div>
									<!-- 価格(昼) -->
									<div class="entry-item-lunch entry-item-price">
										<i class="fas fa-sun"></i>
										<?php
											$lunchPrices = my_the_post_taxonomy($post->ID, 'lunch_prices');
											if (!empty($lunchPrices)) {
												echo '<p> ¥' . number_format($lunchPrices[0]->name) . ' ~ ¥' . number_format($lunchPrices[1]->name) . '</p>';
											} else {
												echo '-';
											}
										?>
									</div>
								</div>
							</div><!-- /entry-item-meta -->
							<div class="entry-item-excerpt">
								<?php the_excerpt();?>
							</div>
						</div><!-- /entry-item-body -->
					</a><!-- /entry-item -->

					<?php
					endwhile;
					?>
				</div>
				<?php endif; ?>
				<!-- pagenation -->
				<?php get_template_part('template-parts/pagenation');?>
			</main><!-- /primary -->

			<?php get_sidebar(); ?>


		</div><!-- /inner -->
	</div><!-- /content -->

  <?php get_footer(); ?>
