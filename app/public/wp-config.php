<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4HmU8ddaWmbqlV5o6n96ZkUJsVn+kyqbwZs1Q4lSbCz12OZVx2Vo7hP2KguCXx0SQIhAZxhxsgolLjZ9dPuZYg==');
define('SECURE_AUTH_KEY',  '4D+ci4T2kAu+bl3qgnTVbJhcNWdijAE3bbKhhH3WydVfXLBdc7jOqwsSiWmdzTivDwF7OR+qDhaF+ugrN1xKGA==');
define('LOGGED_IN_KEY',    '8ziIDOPU9krrOh4TkyaUthgomLh0Se7/PyDpndka9Hqb0ICGfVGHkaahnTzKbpgPkdOQ/xZkXOcN+DznG0LnWA==');
define('NONCE_KEY',        'NXW5Av5eWx7d6tIVe3jn9xyyvWlpSFISGhE8zv8j9BCIxdW3A0VPoKbMJrEsnBCpXYrff+x/oEgKnVvEsCa9hA==');
define('AUTH_SALT',        'XKQp7wkhD16l0GARRxEbIx+m7uj7getzK0cG7CjiVJC1MjBARGCDYWKWwYEm0AuapQTPUHZ2HBm0+bj2WqNNig==');
define('SECURE_AUTH_SALT', 'JfylR2/Emmx+hvz19YUH8TckysLZ7BY2BoDKWe9J6ivy8ad1e/9UL+DBegpD+0Ee2nmcCK0bKPfUMiOatsGR3w==');
define('LOGGED_IN_SALT',   'NaMPBqYtKzIcbW6kTCcz01rG6CJ/RaWrxo7YF0JZOpfm7lAMdE70RSK+1HoQTmH6dCnI/Tr8jIehgKX8Fxw62Q==');
define('NONCE_SALT',       'U04lOCbF1mGDQWvyya47donJXWeARhCprjhr5Xig614bjKFeVJ+Gs+TfgXsnSzHAuo5oZEUhmn0B+hUBD/K72Q==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
